//
//  NumberView.m
//  Graph
//
//  Created by Dmitry Simkin on 08.04.13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "TextView.h"

@implementation TextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor greenColor];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame text:(NSString *)text
{
    self = [self initWithFrame:frame];
    if (self)
    {
        self.text = text;
    }
    return self;
}

- (void)setText:(NSString *)text
{
    _text = text;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    float fontSize = 50;
    UIFont *font = nil;
    CGSize size = CGSizeZero;
    while (true)
    {
        font = [UIFont boldSystemFontOfSize:fontSize];
        size = [[self text] sizeWithFont:font];
        if (size.width < self.frame.size.width && size.height < self.frame.size.height)
        {
            break;
        }
        fontSize--;
    }
    
    CGPoint drawPoint = CGPointMake((self.frame.size.width - size.width) / 2 , (self.frame.size.height - size.height) / 2);
    
    [self.text drawAtPoint:drawPoint withFont:font];
}

@end
