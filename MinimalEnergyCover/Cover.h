//
//  Cover.h
//  MinimalEnergyCover
//
//  Created by Dmitry Simkin on 5/17/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Module.h"

@interface Cover : NSObject

@property (nonatomic, strong) NSArray *functions;
@property (nonatomic, strong) NSMutableArray *modules;
@property (nonatomic) float energy;

- (void)addModule:(Module *)module;
- (id)initWithModules:(NSArray *)modules;
- (Cover *)mutableCopy;

@end
