//
//  MatrixView.m
//  Graph
//
//  Created by Dmitry Simkin on 4/7/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "MatrixView.h"

#import "MatrixCellView.h"

#import "TextView.h"

@interface MatrixView() <MatrixCellViewDelegate>

@end

@implementation MatrixView

static const float kLeftOffset = 40;
static const float kRightOffset = 40;
static const float kTopOffset = 40;
static const float kBottomOffset = 40;

- (NSMutableArray *)functionsHeaders
{
    if (!_functionsHeaders)
    {
        _functionsHeaders = [NSMutableArray array];
    }
    return _functionsHeaders;
}

- (NSMutableArray *)modulesHeaders
{
    if (!_modulesHeaders)
    {
        _modulesHeaders = [NSMutableArray array];
    }
    return _modulesHeaders;
}

- (NSMutableArray *)energyHeaders
{
    if (!_energyHeaders)
    {
        _energyHeaders = [NSMutableArray array];
    }
    return _energyHeaders;
}

- (void)createMatrixViews
{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:self.size.width * self.size.height];
    
    for (int i = 0 ; i < self.size.width * self.size.height; i++)
    {
        MatrixCellView *cellView = [[MatrixCellView alloc] init];
        cellView.delegate = self;
        [array addObject:cellView];
    }
    self.matrixViews = array;
}

- (void)showWithMatrix:(NSArray *)matrix
{
    [self clearMatrix];
    [self createMatrixViews];
    [self showMatrix];
    [self syncMatrixViewsViewMatrix:matrix];
}

- (void)syncMatrixViewsViewMatrix:(NSArray *)matrix
{
    for (int i = 0; i < matrix.count; i++)
    {
        MatrixCell *cell = (MatrixCell *)[matrix objectAtIndex:i];
        MatrixCellView *cellView = (MatrixCellView *)[self.matrixViews objectAtIndex:i];
        cellView.cell = cell;
    }
}

- (void)clearMatrix
{
    for (int i = 0; i < self.matrixViews.count; i++)
        [(MatrixCellView *)[self.matrixViews objectAtIndex:i] removeFromSuperview];
    
    self.matrixViews = nil;
    
    for (TextView *header in self.functionsHeaders)
         [header removeFromSuperview];
    
    [self.functionsHeaders removeAllObjects];
    
    for (TextView *header in self.modulesHeaders)
        [header removeFromSuperview];
    
    [self.modulesHeaders removeAllObjects];
    
    for (TextView *header in self.energyHeaders)
        [header removeFromSuperview];
    
    [self.energyHeaders removeAllObjects];
}

- (void)showModulesHeader
{
    for (int i = 0; i < self.size.height; i++)
    {
        NSString *text = [NSString stringWithFormat:@"m%i", i + 1];
        TextView *moduleHeader = [[TextView alloc] initWithFrame:CGRectMake(0, [self topOffset] + i * [self cellHeight], [self leftOffset], [self cellHeight]) text:text];
        [self addSubview:moduleHeader];
        [self.modulesHeaders addObject:moduleHeader];
    }
}

- (void)showFunctionsHeader
{
    for (int i = 0; i < self.size.width; i++)
    {
        NSString *text = [NSString stringWithFormat:@"f%i", i + 1];
        TextView *functionHeader = [[TextView alloc] initWithFrame:CGRectMake([self leftOffset] + i * [self cellWidth], 0, [self cellWidth], [self topOffset]) text:text];
        [self addSubview:functionHeader];
        [self.functionsHeaders addObject:functionHeader];
    }
}

- (void)showEnergyHeaders
{
    NSArray *energies = [self.dataSource matrixViewEnergies];
    if (energies.count == 0)
        return;
    
    for (int i = 0; i < self.size.height; i++)
    {
        NSString *text = [NSString stringWithFormat:@"%.2f", [[energies objectAtIndex:i] floatValue]];
        TextView *energyHeader = [[TextView alloc] initWithFrame:CGRectMake(self.frame.size.width - [self rightOffset], [self topOffset] + i * [self cellHeight], [self leftOffset], [self cellHeight]) text:text];
        [self addSubview:energyHeader];
        [self.energyHeaders addObject:energyHeader];
    }
}

- (void)showMatrix
{
    float cellWidth = [self cellWidth];
    float cellHeight = [self cellHeight];
    float xOffset = [self leftOffset];
    float yOffset = [self topOffset];
    
    [self showModulesHeader];
    [self showFunctionsHeader];
    [self showEnergyHeaders];
    
    for (int i = 0; i < self.size.height * self.size.width; i++)
    {
        int row = (int)(i / self.size.width);
        int column = (int)(i % (int)(self.size.width));
        MatrixCellView *cellView = (MatrixCellView *)[self.matrixViews objectAtIndex:i];
        cellView.frame = CGRectMake(xOffset + column * cellWidth, yOffset + row * cellHeight, cellWidth, cellHeight);
        [self addSubview:cellView];
    }
}

- (void)updateEnergiesHeaders
{
    NSArray *energies = [self.dataSource matrixViewEnergies];
    
    for (int i = 0; i < energies.count; i++)
    {
        TextView *header = (TextView *)[self.energyHeaders objectAtIndex:i];
        header.text = [NSString stringWithFormat:@"%i", [[energies objectAtIndex:i] integerValue]];
    }
}

- (float)leftOffset
{
    return kLeftOffset;
}

- (float)rightOffset
{
    return kRightOffset;
}

- (float)topOffset
{
    return kTopOffset;
}

- (float)bottomOffset
{
    return kBottomOffset;
}

- (float)cellWidth
{
    return ((self.frame.size.width - [self leftOffset] - [self rightOffset]) / self.size.width);
}

- (float)cellHeight
{
    return ((self.frame.size.height - [self topOffset] - [self bottomOffset]) / self.size.height);
}

#pragma mark - Matrix cell delegate

- (void)matrixCellView:(MatrixCellView *)matrixCellView didValueChanged:(NSInteger)value
{
    [self.delegate matrixView:self cellValueChanged:matrixCellView.cell];
}

@end
