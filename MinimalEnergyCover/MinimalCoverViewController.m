//
//  MinamalCoverViewController.m
//  Graph
//
//  Created by Dmitry Simkin on 4/7/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "MinimalCoverViewController.h"

#import "MatrixView.h"
#import "MatrixCellView.h"
#import "TextView.h"

#import "Module.h"

#import "AlgorithmModel.h"

typedef enum
{
    PickerComponentRows = 2,	
    PickerComponentColumns = 0
} PickerComponentNumber;

@interface MinimalCoverViewController () <UIPickerViewDataSource, UIPickerViewDelegate, MatrixViewDataSource, MatrixViewDelegate>

@property (weak, nonatomic) IBOutlet MatrixView *matrixContainerView;
@property (weak, nonatomic) IBOutlet UIPickerView *matrixSizePicker;
@property (weak, nonatomic) IBOutlet UITextView *consoleTextView;

@property (nonatomic, strong) NSArray *sizes;

@property (nonatomic, strong) AlgorithmModel *model;

@end

@implementation MinimalCoverViewController

static const int kMinMatrixSize = 4;
static const int kMaxMatrixSize = 41;
static const int kMatrixDefaultSize = 8;

static const int kMiddlePickerComponentWidth = 40;

- (void)awakeFromNib
{
    NSMutableArray *sizes = [NSMutableArray array];
    for (int i = kMinMatrixSize; i < kMaxMatrixSize; i++)
    {
        [sizes addObject:[NSString stringWithFormat:@"%i", i]];
    }
    self.sizes = [sizes copy];
    
    self.model = [[AlgorithmModel alloc] init];
}

- (IBAction)generateButtonPressed:(id)sender
{
    self.model.matrix = [self generateMatrixWithPercentage:35];
    [self.matrixContainerView showWithMatrix:self.model.matrix];
}

- (IBAction)calculateButtonPressed:(id)sender
{
    self.consoleTextView.text = nil;
    [self.model findMinimalCoverByOptimalRecursiveAlgorithm];
    [self showCover];
}

- (void)showCover
{
    NSArray *matrixViews = self.matrixContainerView.matrixViews;
    NSArray *coveredFunctions = [NSArray array];
    
    for (Module *module in self.model.minCover)
    {
        TextView *moduleView = [self.matrixContainerView.modulesHeaders objectAtIndex:module.number];
        moduleView.backgroundColor = [UIColor blueColor];
        
        TextView *energyHeader = [self.matrixContainerView.energyHeaders objectAtIndex:module.number];
        energyHeader.backgroundColor = [UIColor blueColor];
        
        NSArray *functionsToHightLight = [self.model substractFromArray:[self.model F:@[module]] array:coveredFunctions];
        NSArray *array = @[@{@"functions" : coveredFunctions}, @{@"functions" : functionsToHightLight}];
        coveredFunctions = [array valueForKeyPath:@"@distinctUnionOfArrays.functions"];
        
        for (NSString *function in functionsToHightLight)
        {
            int value = [function integerValue];
            MatrixCellView *cell = [matrixViews objectAtIndex:module.number * self.matrixContainerView.size.width + value];
            cell.backgroundColor = [UIColor redColor];
        }
    }
    
    NSString *consoleOutputText = @"Path for finding minimal energy cover:\n";
    
    for (NSArray *cover in self.model.covers)
    {
        NSString *coverString;
        
        if (cover == self.model.minCover)
            coverString = @"\nmin cover:";
        else
            coverString = @"cover:";
        
        for (int i = 0; i < cover.count; i++)
        {
            Module *m = [cover objectAtIndex:i];
            if (i != cover.count - 1)
                coverString = [coverString stringByAppendingFormat:@" m%i,", m.number + 1];
            else 
                coverString = [coverString stringByAppendingFormat:@" m%i", m.number + 1];
        }
        
        coverString = [coverString stringByAppendingFormat:@": %.2f", [self.model spentEnergyFromCover:cover]];
        consoleOutputText = [consoleOutputText stringByAppendingFormat:@"%@\n", coverString];
    }

    self.consoleTextView.text = consoleOutputText;
}

- (CGSize)size
{
    int rowsCount = [[self.sizes objectAtIndex:[self.matrixSizePicker selectedRowInComponent:PickerComponentRows]] intValue];
    int columnsCount = [[self.sizes objectAtIndex:[self.matrixSizePicker selectedRowInComponent:PickerComponentColumns]] intValue];

    return CGSizeMake(columnsCount, rowsCount);
}

#pragma mark - MatrixGenerator

- (NSArray *)generateMatrixWithPercentage:(float)percentage
{
    NSArray *matrix = [self emptyMatrix];
    int maxRibCount = self.size.width * self.size.height;
    
    int ribCount = (int)(maxRibCount * percentage / 100);
    
    for (int i = 0; i < ribCount; i++)
    {
        while (YES)
        {
            int row = (int)(rand() % (int)(self.size.height));
            int column = (int)(rand() % (int)(self.size.width));
            
            int index = row * self.size.width + column;
            if (index >= matrix.count)
            {
                NSLog(@"row %i, column %i, size:%@", row, column, NSStringFromCGSize(self.size));
            }
            MatrixCell *cell = [matrix objectAtIndex:index];
            
            if (cell.value)
                continue;
            else
            {
                cell.value = 1;
                break;
            }
        }
    }
    return matrix;
}

- (NSArray *)emptyMatrix
{
    NSMutableArray *matrix = [NSMutableArray array];
    for (int i = 0; i < self.size.width * self.size.height; i++)
    {
        int row = (int)(i / self.size.width);
        int column = (int)(i % (int)(self.size.width));
        [matrix addObject:[[MatrixCell alloc] initWithRow:row column:column]];
    }
    return  matrix;
}

#pragma mark - Picker

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == PickerComponentRows || component == PickerComponentColumns)
        return self.sizes.count;
    else
        return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == PickerComponentRows || component == PickerComponentColumns)
        return [self.sizes objectAtIndex:row];
    else
        return @"x";
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (component == PickerComponentRows || component == PickerComponentColumns)
        return (pickerView.frame.size.width - kMiddlePickerComponentWidth) / 2;
    else
        return kMiddlePickerComponentWidth;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.matrixContainerView.size = [self size];
    self.model.size = [self size];
    [self.matrixContainerView showWithMatrix:self.model.matrix];
}

#pragma mark - MatrixViewDelegate

- (void)matrixView:(MatrixView *)matrixView cellValueChanged:(MatrixCell *)cell
{
    [self.model updateCellWithValue:cell.value atRow:cell.row column:cell.column];
    [self.matrixContainerView showWithMatrix:self.model.matrix];
    self.consoleTextView.text = nil;
}

#pragma mark - MatrixViewDataSource

- (NSArray *)matrixViewEnergies
{
    NSMutableArray *energies = [NSMutableArray array];
    for (Module *m in self.model.modules)
    {
        [energies addObject:[NSNumber numberWithFloat:m.multiplier]];
    }
    return energies;
}
#pragma mark - View controller lifecycle

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.matrixContainerView showWithMatrix:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.matrixContainerView.dataSource = self;
    self.matrixContainerView.delegate = self;
    
    [self.matrixSizePicker selectRow:(kMatrixDefaultSize - kMinMatrixSize) inComponent:PickerComponentRows animated:NO];
    [self.matrixSizePicker selectRow:(kMatrixDefaultSize - kMinMatrixSize) inComponent:PickerComponentColumns animated:NO];
    
    self.model.size = [self size];
    self.matrixContainerView.size = [self size];
}

@end
