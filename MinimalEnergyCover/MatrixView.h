//
//  MatrixView.h
//  Graph
//
//  Created by Dmitry Simkin on 4/7/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MatrixCell.h"

@class TextView;

@class MatrixView;

@protocol MatrixViewDelegate <NSObject>

- (void)matrixView:(MatrixView *)matrixView cellValueChanged:(MatrixCell *)cell;

@end

@protocol MatrixViewDataSource <NSObject>

- (NSArray *)matrixViewEnergies;

@end

@interface MatrixView : UIView

@property (nonatomic) CGSize size;

@property (nonatomic, strong) NSArray *matrixViews;

@property (nonatomic, strong) NSMutableArray *modulesHeaders;
@property (nonatomic, strong) NSMutableArray *functionsHeaders;
@property (nonatomic, strong) NSMutableArray *energyHeaders;

- (void)showWithMatrix:(NSArray *)matrix;
- (void)updateEnergiesHeaders;

@property (nonatomic, weak) id <MatrixViewDataSource> dataSource;
@property (nonatomic, weak) id <MatrixViewDelegate> delegate;

@end
