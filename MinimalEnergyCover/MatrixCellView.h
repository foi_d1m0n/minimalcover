//
//  MatrixCell.h
//  Graph
//
//  Created by Dmitry Simkin on 3/23/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MatrixCell.h"

@class MatrixCellView;

@protocol MatrixCellViewDelegate

- (void)matrixCellView:(MatrixCellView *)matrixCellView didValueChanged:(NSInteger)value;

@end

@interface MatrixCellView : UIView

@property (nonatomic, weak) id <MatrixCellViewDelegate> delegate;
@property (nonatomic, strong) MatrixCell *cell;

- (id)initWithCell:(MatrixCell *)cell delegate:(id <MatrixCellViewDelegate>)delegate;
- (id)initWithFrame:(CGRect)frame delegate:(id <MatrixCellViewDelegate>)delegate;

@end

