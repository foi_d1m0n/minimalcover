//
//  Cover.m
//  MinimalEnergyCover
//
//  Created by Dmitry Simkin on 5/17/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "Cover.h"

@implementation Cover

- (id)init
{
    self = [super init];
    if (self)
    {
        self.modules = [NSMutableArray array];
        self.functions = [NSArray array];
        self.energy = [self recountEnergy];
        self.functions = [self F:self.modules];
    }
    return self;
}

- (id)initWithModules:(NSArray *)modules
{
    self = [self init];
    if (self)
    {
        self.modules = [modules mutableCopy];
    }
    return self;
}

- (Cover *)mutableCopy
{
    Cover *cover = [[Cover alloc] init];
    cover.functions = self.functions;
    cover.modules = [self.modules mutableCopy];
    cover.energy = self.energy;
    return cover;
}

- (void)addModule:(Module *)module
{
    [self.modules addObject:module];
    self.energy = [self recountEnergy];
    self.functions = [self F:self.modules];
}

- (float)recountEnergy
{
    float energy = 0;
    
    if (self.modules.count)
    {
        Module *m1 = (Module *)[self.modules objectAtIndex:0];
        NSArray *coveredFunctions = [m1.functions keysSortedByValueUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            if ([obj1 integerValue] > [obj2 integerValue]) {
                return (NSComparisonResult)NSOrderedDescending;
            }
            
            if ([obj1 integerValue] < [obj2 integerValue]) {
                return (NSComparisonResult)NSOrderedAscending;
            }
            return (NSComparisonResult)NSOrderedSame;
        }];
        for (Module *m in self.modules)
        {
            NSArray *functionsToHightLight = [self intersceptFromArray:[self F:@[m]] array:coveredFunctions];
            coveredFunctions = [self substractFromArray:coveredFunctions array:[self F:@[m]]];
            
            energy += [m energyFromFunctions:functionsToHightLight];
        }
    }
    return energy;
}

- (NSArray *)intersceptFromArray:(NSArray *)left array:(NSArray *)right
{
    NSMutableArray *result = [NSMutableArray array];
    
    for (NSString *lf in left)
    {
        for (NSString *rf in right)
        {
            if ([lf isEqualToString:rf])
            {
                [result addObject:lf];
                break;
            }
        }
    }
    
    return result;
}

- (NSArray *)substractFromArray:(NSArray *)left array:(NSArray *)right
{
    NSMutableArray *result = [left mutableCopy];
    
    for (NSString *rf in right)
    {
        for (NSString *lf in left)
        {
            if ([rf isEqualToString:lf])
            {
                [result removeObject:lf];
                break;
            }
        }
    }
    
    return result;
}

- (NSArray *)F:(NSArray *)modules
{
    NSMutableArray *functions = [NSMutableArray array];
    for (NSString *f in self.functions)
    {
        for (Module *m in modules)
        {
            int value = [[m.functions valueForKey:f] integerValue];
            if (value == 1)
            {
                [functions addObject:f];
                break;
            }
        }
    }
    
    return functions;
}

@end
