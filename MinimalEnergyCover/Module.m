//
//  Module.m
//  Graph
//
//  Created by Dmitry Simkin on 4/7/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "Module.h"

@implementation Module

- (id)initWithNumber:(NSInteger)number
{
    self = [super init];
    if (self)
    {
        self.number = number;
        self.multiplier = (1.0 + (rand() % 19)) / 20.0;
        self.functions = [NSMutableDictionary dictionary];
    }
    return self;
}

- (float)energy
{
    float energy = 0;
    for (int i = 0; i < self.functions.count; i++)
    {
        int value = [[self.functions valueForKey:[NSString stringWithFormat:@"%i", i]] integerValue];
        if (value == 1)
            energy += self.multiplier;
    }
    return energy;
}

- (float)energyFromFunctions:(NSArray *)functions
{
    float energy = 0;
    for (NSString *f in functions)
    {
        int value = [[self.functions valueForKey:f] integerValue];
        if (value == 1)
            energy += self.multiplier;
    }
    return energy;
//    return self.multiplier * functions.count;
}

@end
