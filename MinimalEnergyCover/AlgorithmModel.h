//
//  AlgorithmModel.h
//  MinimalEnergyCover
//
//  Created by Dmitry Simkin on 5/12/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlgorithmModel : NSObject

@property (nonatomic, strong) NSArray *matrix;
@property (nonatomic, strong) NSMutableArray *modules;
@property (nonatomic, strong) NSMutableArray *minCover;
@property (nonatomic, strong) NSMutableArray *covers;
@property (nonatomic, strong) NSMutableArray *functions;

@property (nonatomic) CGSize size;

- (void)findMinimalCoverByOptimalRecursiveAlgorithm;

- (NSArray *)substractFromArray:(NSArray *)left array:(NSArray *)right;
- (NSArray *)F:(NSArray *)modules;
- (float)spentEnergyFromCover:(NSArray *)cover;
- (void)updateCellWithValue:(NSInteger)value atRow:(NSInteger)row column:(NSInteger)column;

@end
