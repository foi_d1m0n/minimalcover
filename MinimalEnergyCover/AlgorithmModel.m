//
//  AlgorithmModel.m
//  MinimalEnergyCover
//
//  Created by Dmitry Simkin on 5/12/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "AlgorithmModel.h"

#import "Module.h"
#import "MatrixCell.h"

@implementation AlgorithmModel

- (void)findMinimalCoverByOptimalRecursiveAlgorithm
{
//    [self prepareModelWithMatrix:self.matrix];

    self.minCover = self.modules;
    NSMutableArray *initialCover = [NSMutableArray array];
    self.covers = [NSMutableArray array];

    [self minCoverFromCover:[initialCover mutableCopy] andModules:[self.modules mutableCopy]];
}

- (void)setMatrix:(NSArray *)matrix
{
    _matrix = matrix;
    [self prepareModelWithMatrix:_matrix];
}

- (void)setSize:(CGSize)size
{
    _size = size;
    self.matrix = [self emptyMatrix];
    [self prepareModelWithMatrix:self.matrix];
}

- (NSArray *)emptyMatrix
{
    NSMutableArray *matrix = [NSMutableArray array];
    for (int i = 0; i < self.size.width * self.size.height; i++)
    {
        int row = (int)(i / self.size.width);
        int column = (int)(i % (int)(self.size.width));
        [matrix addObject:[[MatrixCell alloc] initWithRow:row column:column]];
    }
    return  matrix;
}

- (void)updateCellWithValue:(NSInteger)value atRow:(NSInteger)row column:(NSInteger)column
{
    MatrixCell *cell = (MatrixCell *)[self.matrix objectAtIndex:row * self.size.width + column];
    cell.value = value;
    [self prepareModelWithMatrix:self.matrix];
}

- (void)prepareModelWithMatrix:(NSArray *)theMatrix
{
    int rowsCount = (int)self.size.height;
    int columnsCount = (int)self.size.width;
    
    self.modules = [NSMutableArray array];
    for (int i = 0; i < rowsCount; i++)
    {
        Module *module = [[Module alloc] initWithNumber:i];
        NSRange range = NSMakeRange(i * columnsCount, columnsCount);
        NSArray *row = [theMatrix subarrayWithRange:range];
        
        for (MatrixCell *cell in row)
            [module.functions setValue:[NSNumber numberWithInt:cell.value] forKey:[NSString stringWithFormat:@"%i", cell.column]];
        
        [self.modules addObject:module];
    }
    
//    self.energies = energies;
    
    self.functions = [NSMutableArray array];
    for (int i = 0; i < columnsCount; i++)
    {
        [self.functions addObject:[NSString stringWithFormat:@"%i", i]];
    }
}

- (void)minCoverFromCover:(NSMutableArray *)cover andModules:(NSMutableArray *)modules
{
    if ([self substractFromArray:self.functions array:[self F:cover]].count == 0)
    {
        if ([self spentEnergyFromCover:cover] <= [self spentEnergyFromCover:self.minCover])
        {
            if (cover.count < self.minCover.count)
            {
                NSArray *sortedCover = [cover sortedArrayUsingComparator:^NSComparisonResult(Module *m1, Module *m2) {
                    if (m1.multiplier > m2.multiplier)
                        return (NSComparisonResult)NSOrderedDescending;
                    if (m1.multiplier < m2.multiplier)
                        return (NSComparisonResult)NSOrderedAscending;
                    return (NSComparisonResult)NSOrderedSame;
                }];
                [self.covers addObject:sortedCover];
                self.minCover = [sortedCover mutableCopy];
            }
        }
    }
    else
    {
        if ([self spentEnergyFromCover:cover] <= [self spentEnergyFromCover:self.minCover] )
        {
            for (Module *m in modules)
            {
                if ([self substractFromArray:[self F:@[m]] array:[self F:cover]].count != 0)
                {
                    NSMutableArray *newCover = [cover mutableCopy];
                    [newCover addObject:m];
                    NSMutableArray *newModules = [modules mutableCopy];
                    [newModules removeObject:m];

                    [self minCoverFromCover:newCover andModules:newModules];
                }
            }
        }
    }
}

- (float)spentEnergyFromCover:(NSArray *)cover
{
    float energy = 0;
    
    if (cover.count)
    {
//        Module *m1 = (Module *)[cover objectAtIndex:0];
//        NSArray *coveredFunctions = [m1.functions keysSortedByValueUsingComparator:^NSComparisonResult(id obj1, id obj2) {
//            if ([obj1 integerValue] > [obj2 integerValue]) {
//                return (NSComparisonResult)NSOrderedDescending;
//            }
//            
//            if ([obj1 integerValue] < [obj2 integerValue]) {
//                return (NSComparisonResult)NSOrderedAscending;
//            }
//            return (NSComparisonResult)NSOrderedSame;
//        }];
        NSArray *coveredFunctions = [self.functions copy];
        NSArray *sortedCover = [cover sortedArrayUsingComparator:^NSComparisonResult(Module *m1, Module *m2) {
            if (m1.multiplier > m2.multiplier)
                return (NSComparisonResult)NSOrderedDescending;
            if (m1.multiplier < m2.multiplier)
                return (NSComparisonResult)NSOrderedAscending;
            return (NSComparisonResult)NSOrderedSame;
        }];
        for (Module *m in sortedCover)
        {
            if (coveredFunctions.count != 0)
            {
                NSArray *functionsToHightLight = [self intersceptFromArray:[self F:@[m]] array:coveredFunctions];
                coveredFunctions = [self substractFromArray:coveredFunctions array:[self F:@[m]]];
                
                energy += [m energyFromFunctions:functionsToHightLight];
            }
            else
                break;
        }
    }
    return energy;
}


- (NSArray *)intersceptFromArray:(NSArray *)left array:(NSArray *)right
{
    NSMutableArray *result = [NSMutableArray array];
    
    for (NSString *lf in left)
    {
        for (NSString *rf in right)
        {
            if ([lf isEqualToString:rf])
            {
                [result addObject:lf];
                break;
            }
        }
    }
    
    return result;
}

- (NSArray *)substractFromArray:(NSArray *)left array:(NSArray *)right
{
    NSMutableArray *result = [left mutableCopy];
    
    for (NSString *rf in right)
    {
        for (NSString *lf in left)
        {
            if ([rf isEqualToString:lf])
            {
                [result removeObject:lf];
                break;
            }
        }
    }
    
    return result;
}

- (NSArray *)F:(NSArray *)modules
{
    NSMutableArray *functions = [NSMutableArray array];
    for (NSString *f in self.functions)
    {
        for (Module *m in modules)
        {
            int value = [[m.functions valueForKey:f] integerValue];
            if (value == 1)
            {
                [functions addObject:f];
                break;
            }
        }
    }
    
    return functions;
}


@end