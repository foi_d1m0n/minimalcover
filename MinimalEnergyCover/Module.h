//
//  Module.h
//  Graph
//
//  Created by Dmitry Simkin on 4/7/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Module : NSObject

@property (nonatomic, strong) NSMutableDictionary *functions;
@property (nonatomic) NSInteger number;
@property (nonatomic) float multiplier;

- (id)initWithNumber:(NSInteger)number;
- (float)energy;
- (float)energyFromFunctions:(NSArray *)functions;

@end
