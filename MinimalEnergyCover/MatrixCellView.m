//
//  MatrixCell.m
//  Graph
//
//  Created by Dmitry Simkin on 3/23/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "MatrixCellView.h"

@interface MatrixCellView()

@end

@implementation MatrixCellView

- (id)initWithCell:(MatrixCell *)cell delegate:(id <MatrixCellViewDelegate>)delegate;
{
    self = [self init];
    if (self)
    {
        self.cell = cell;
        self.delegate = delegate;
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        self.backgroundColor = [UIColor yellowColor];
        
        UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [self addGestureRecognizer:tapGR];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame delegate:(id <MatrixCellViewDelegate>)delegate;
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.delegate = delegate;
    }
    return self;
}

- (void)setCell:(MatrixCell *)cell
{
    _cell = cell;
    [self setNeedsDisplay];
}

- (void)tapped:(UITapGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateRecognized)
    {
        if  (self.cell.value == 1)
            self.cell.value = 0;
        else
            self.cell.value = 1;

        [self setNeedsDisplay];
        [self.delegate matrixCellView:self didValueChanged:self.cell.value];
    }
}

- (void)drawRect:(CGRect)rect
{
    NSString *valueString = [NSString stringWithFormat:@"%i", self.cell.value];
    UIFont *font = [UIFont systemFontOfSize:18.0];
    CGSize size = [valueString sizeWithFont:font];
    
    [valueString drawAtPoint:CGPointMake((self.frame.size.width - size.width) / 2, (self.frame.size.height - size.height) / 2) withFont:font];
}

@end
