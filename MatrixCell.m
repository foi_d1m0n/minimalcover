//
//  MatrixCell.m
//  MinimalEnergyCover
//
//  Created by Dmitry Simkin on 5/12/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "MatrixCell.h"

@implementation MatrixCell

- (id)initWithRow:(NSInteger)row column:(NSInteger)column
{
    self = [super init];
    if (self)
    {
        self.row = row;
        self.column = column;
    }
    return self;
}

- (id)initWithRow:(NSInteger)row column:(NSInteger)column value:(NSInteger)value
{
    self = [super init];
    if (self)
    {
        self.row = row;
        self.column = column;
        self.value = value;
    }
    return self;
}

@end
