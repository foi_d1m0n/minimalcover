//
//  MatrixCell.h
//  MinimalEnergyCover
//
//  Created by Dmitry Simkin on 5/12/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MatrixCell : NSObject

@property (nonatomic) NSInteger row;
@property (nonatomic) NSInteger column;
@property (nonatomic) NSInteger value;

- (id)initWithRow:(NSInteger)row column:(NSInteger)column;
- (id)initWithRow:(NSInteger)row column:(NSInteger)column value:(NSInteger)value;

@end
